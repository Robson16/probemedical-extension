import { __ } from '@wordpress/i18n';
import axios from 'axios';

export default class PreEvaluationForm {
	constructor() {
		this.preEvaluation = document.querySelectorAll( '.pme-pre-evaluation' );
		this.preEvaluationForms = document.querySelectorAll(
			'.pme-pre-evaluation form'
		);
		this.examplesModalToggle = document.querySelectorAll(
			'[data-pme-toggle="modal"]'
		);

		this.events();
	}

	// Events
	events() {
		this.handleToggleExamplesModal();
		this.handleAllQuestionsHasAnswer();
		this.handleAnswerChange();
		this.handleFormSubmit();
	}

	// Methods
	handleToggleExamplesModal() {
		this.examplesModalToggle.forEach( ( toggler ) => {
			toggler.addEventListener( 'click', ( event ) => {
				const targetModal = event.target.dataset.pmeTarget;
				document
					.getElementById( targetModal )
					.classList.toggle( 'show' );
			} );
		} );
	}

	handleFormSubmit() {
		this.preEvaluationForms.forEach( ( form ) => {
			form.addEventListener( 'submit', async ( event ) => {
				event.preventDefault();

				const targetForm = event.target;
				const spinner = targetForm.querySelector( '.spinner' );
				const responseOutput = targetForm.querySelector(
					'.response-output'
				);

				spinner.classList.add( 'visible' );

				const formDataObject = new FormData( targetForm );
				const formData = this.serialize( formDataObject );

				const response = await axios.post(
					'/wp-json/probemedicalext/v1/sendmail',
					{
						contact_name: formData[ 'your-name' ],
						contact_email: formData[ 'your-email' ],
						contact_subject: __(
							'New pre-evaluation request',
							'probemedicalext'
						),
						contact_message: this.emailMessageTemplate( formData ),
					}
				);

				spinner.classList.remove( 'visible' );

				if ( response.status === 200 ) {
					responseOutput.innerHTML = __(
						'We appreciate your message. We will review your order and get in touch with you.',
						'probemedicalext'
					);
					responseOutput.classList.add( 'success' );
					responseOutput.classList.remove( 'error' );
					responseOutput.classList.remove( 'warning' );
				} else if ( response.status === 304 ) {
					responseOutput.innerHTML = __(
						'An error occurred while trying to send your message. Try again later.',
						'probemedicalext'
					);
					responseOutput.classList.add( 'error' );
					responseOutput.classList.remove( 'success' );
					responseOutput.classList.remove( 'warning' );
				} else {
					responseOutput.innerHTML = __(
						'An error occurred while trying to send your message. Try again later.',
						'probemedicalext'
					);
					responseOutput.classList.add( 'warning' );
					responseOutput.classList.remove( 'success' );
					responseOutput.classList.remove( 'error' );
					console.warn(
						response.status + ': ' + response.data.message
					);
				}
			} );
		} );
	}

	handleAllQuestionsHasAnswer() {
		this.preEvaluationForms.forEach( ( form ) => {
			form.addEventListener( 'change', () => {
				if ( ! form.classList.contains( 'all-questions-filled' ) ) {
					const questionsCount = form.querySelectorAll(
						'.question-item'
					).length;
					const questionsWithAnswer = form.querySelectorAll(
						'.question-answer__input:checked'
					).length;

					if ( questionsCount === questionsWithAnswer ) {
						form.classList.add( 'all-questions-filled' );
						form.querySelector( '.submit-modal' ).classList.add(
							'show'
						);
					}
				}
			} );
		} );
	}

	handleAnswerChange() {
		this.preEvaluationForms.forEach( ( form ) => {
			form.addEventListener( 'change', ( event ) => {
				const answerInput = event.target;

				// Change icons over the image
				const targetIcon = document.getElementById(
					answerInput.dataset.targetIcon
				);
				targetIcon.innerHTML = '';

				if ( answerInput.value === 'yes' ) {
					targetIcon.classList.add( 'positive' );
					targetIcon.classList.remove( 'negative' );
				}

				if ( answerInput.value === 'no' ) {
					targetIcon.classList.add( 'negative' );
					targetIcon.classList.remove( 'positive' );
				}

				// Show submit modal
				if (
					answerInput.classList.contains(
						'question-answer__input'
					) &&
					form.classList.contains( 'all-questions-filled' )
				) {
					form.querySelector( '.submit-modal' ).classList.add(
						'show'
					);
				}
			} );
		} );
	}

	serialize( data ) {
		let obj = {};
		for ( let [ key, value ] of data ) {
			if ( obj[ key ] !== undefined ) {
				if ( ! Array.isArray( obj[ key ] ) ) {
					obj[ key ] = [ obj[ key ] ];
				}
				obj[ key ].push( value );
			} else {
				obj[ key ] = value;
			}
		}
		return obj;
	}

	emailMessageTemplate( data ) {
		const questionsTotal = Number( data[ 'questions-count' ] ) + 1;

		const contactInfos = `
      <p>
        <strong>${ __( 'Name:', 'probemedicalext' ) }</strong>
        ${ data[ 'your-name' ] }
      </p>
      <p>
        <strong>${ __( 'Company:', 'probemedicalext' ) }</strong>
        ${ data[ 'your-company' ] }
      </p>
      <p>
        <strong>${ __( 'Email:', 'probemedicalext' ) }</strong>
        ${ data[ 'your-email' ] }
      </p>
      <p>
        <strong>${ __( 'Phone:', 'probemedicalext' ) }</strong>
        ${ data[ 'your-phone' ] }
      </p>
      <p>
        <strong>${ __( 'Message:', 'probemedicalext' ) }</strong>
        ${ data[ 'your-message' ] }
      </p>
      <p>
        <strong>${ __( 'Equipment:', 'probemedicalext' ) }</strong>
        ${ data[ 'title' ] }
      </p>
      <p>
        <strong>${ __( 'Equipment Serial Code', 'probemedicalext' ) }</strong>
        ${ data[ 'your-equipment-serial-cod' ] }
      </p>
    `;

		let questions = `<h4>${ __(
			'Answers to form questions:',
			'probemedicalext'
		) }</h4>`;

		for ( let index = 1; index < questionsTotal; index++ ) {
			questions = `
        ${ questions }
        <p>
          <span>
            <strong>${ __( 'Question:', 'probemedicalext' ) }</strong>
            ${ data[ `question[${ index }][text]` ] }
          </span>
          <br />
          <span>
            <strong>${ __( 'Answer:', 'probemedicalext' ) }</strong>
            ${
				data[ `question[${ index }][answer]` ] === 'yes'
					? __( 'Yes', 'probemedicalext' )
					: __( 'No', 'probemedicalext' )
			}
          </span>
          <br />
          <span>
            <strong>${ __( 'Selected examples:', 'probemedicalext' ) }</strong>
            ${
				data[ `question[${ index }][example]` ] !== undefined
					? data[ `question[${ index }][example]` ]
					: `${ __( 'None', 'probemedicalext' ) }`
			}
          </span>
        </p>
      `;
		}

		return `${ contactInfos } ${ questions }`;
	}
}
