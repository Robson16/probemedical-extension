<?php

/**
 * Register Custom REST API EndPoint - Sendmail
 *
 */

function probemedicalext_sendmail_callback(WP_REST_Request $request)
{
  $response = array(
    'status'  => 304,
    'message' => __('There was an error sending the form.', 'probemedicalext')
  );

  $siteName = wp_strip_all_tags(trim(get_option('blogname')));
  $contactName    = $request['contact_name'];
  $contactEmail   = $request['contact_email'];
  $contactSubject = $request['contact_subject'];
  $contactMessage = $request['contact_message'];

  $subject = "[$siteName] $contactSubject";
  $body   = "<h3>$subject</h3><br/>";
  $body   .= $contactMessage;

  $to = (get_option('probemedicalext_submit_to')) ? get_option('probemedicalext_submit_to') : get_option('admin_email');
  $headers = array(
    'Content-Type: text/html; charset=UTF-8',
    "Reply-To: $contactName <$contactEmail>",
  );

  if (wp_mail($to, $subject, $body, $headers)) {
    $response['status']   = 200;
    $response['message']  = __('Form sent successfully.', 'probemedicalext');
    $response['test']     = $body;
  }

  return new WP_REST_Response($response);
}

function probemedicalext_sendmail()
{
  register_rest_route('probemedicalext/v1', 'sendmail', array(
    'methods'             => WP_REST_SERVER::CREATABLE,
    'callback'            => 'probemedicalext_sendmail_callback',
    'permission_callback' => function () {
      return true;
    },
    'args'  => array(
      'contact_name'  => array(
        'required'  => true,
        'validate_callback' => function ($value) {
          return preg_match('/[a-z0-9]{2,}/i', $value) ? true :
            new WP_Error('invalid_contact_name', 'Invalid contact name.');
        },
        'sanitize_callback' => 'sanitize_text_field',
      ),
      'contact_email' => array(
        'required'          => true,
        'validate_callback' => 'is_email',
        'sanitize_callback' => 'sanitize_email',
      ),
      'contact_message' => array(
        'required'          => true,
        'sanitize_callback' => 'wp_kses_post', //keeps only HTML tags that are allowed in post content
      ),
    ),
  ));
}

add_action('rest_api_init', 'probemedicalext_sendmail');
