<?php

/**
 * Custom admin top-level menu page
 *
 * @link https://developer.wordpress.org/reference/functions/add_menu_page/
 */

class ProbeMedicalExtensionMenuPages
{
    function __construct()
    {
        add_action('admin_menu', array($this, 'menuPages'));
        add_action('admin_init', array($this, 'settings'));
    }

    function menuPages()
    {
        add_menu_page(
            esc_html__('Probe Medical Extension', 'probemedicalext'), // The text to be displayed in the title tags of the page when the menu is selected.
            esc_html__('Probe Medical Extension', 'probemedicalext'), // The text to be used for the menu.
            'edit_pages', // The capability required for this menu to be displayed to the user.
            'probemedicalext-plugin-options-page', // The slug name to refer to this menu by. Should be unique for this menu page and only include lowercase alphanumeric, dashes, and underscores characters
            null, // The function to be called to output the content for this page.
            plugins_url('assets/svg/icon.svg', dirname(__FILE__)), // The menu icon, can be from Dashicons → https://developer.wordpress.org/resource/dashicons/.
            90 // The position in the menu order this item should appear.
        );

        /**
         * Give the first sub-menu item the same menu slug as the main menu page.
         * This causes the sub-menu item data to replace the data auto-created
         * by adding the main menu page.
         *
         * @link https://wordpress.org/support/topic/creating-an-admin-dashboard-menu-without-a-corresponding-submenu-entry/
         */
        add_submenu_page(
            'probemedicalext-plugin-options-page', // The slug name for the parent menu (or the file name of a standard WordPress admin page).
            esc_html__('Probe Medical Extension ', 'probemedicalext'), // The text to be displayed in the title tags of the page when the menu is selected.
            esc_html__('Settings', 'probemedicalext'), // The text to be used for the menu.
            'edit_pages', // The capability required for this menu to be displayed to the user.
            'probemedicalext-plugin-options-page', // The slug name to refer to this menu by. Should be unique for this menu and only include lowercase alphanumeric, dashes, and underscores characters to be compatible with sanitize_key().
            array($this, 'settingsPageContent'), // The function to be called to output the content for this page.
            0 // The position in the menu order this item should appear.
        );

        add_submenu_page(
            'probemedicalext-plugin-options-page', // The slug name for the parent menu (or the file name of a standard WordPress admin page).
            esc_html__('Mail Settings', 'probemedicalext'), // The text to be displayed in the title tags of the page when the menu is selected.
            esc_html__('Mail Settings', 'probemedicalext'), // The text to be used for the menu.
            'edit_pages', // The capability required for this menu to be displayed to the user.
            'probemedicalext-mail-settings-page', // The slug name to refer to this menu by. Should be unique for this menu and only include lowercase alphanumeric, dashes, and underscores characters to be compatible with sanitize_key().
            array($this, 'mailSettingsPageContent'), // The function to be called to output the content for this page.
            90 // The position in the menu order this item should appear.
        );
    }

    function settings()
    {
        add_settings_section(
            'probemedicalext_mail_settings_section', // Slug-name to identify the section. Used in the 'id' attribute of tags.
            null, // Formatted title of the section. Shown as the heading for the section.
            null, // Function that echos out any content at the top of the section
            'probemedicalext-mail-settings-page' // The slug-name of the settings page on which to show the section.
        );

        add_settings_field(
            'probemedicalext_submit_to', // Slug-name to identify the field. Used in the 'id' attribute of tags.
            esc_html__('Send to', 'probemedicalext'), // Formatted title of the field. Shown as the label for the field during output.
            array($this, 'mailSendToHTML'), // Function that fills the field with the desired form inputs. The function should echo its output.
            'probemedicalext-mail-settings-page', // The slug-name of the settings page on which to show the section
            'probemedicalext_mail_settings_section' // The slug-name of the section of the settings page in which to show the box.
        );

        register_setting(
            'probemedicalext_mail_group', // A settings group name.
            'probemedicalext_submit_to', // The name of an option to sanitize and save.
            array(
                'type' => 'string', // The type of data associated with this setting.
                'description' => esc_html__('List of email addresses', 'probemedicalext'), // A description of the data attached to this setting.
                'sanitize_callback' => array($this, 'sanitizeEmailList'), // A callback function that sanitizes the option's value.
                'default' => esc_html(get_option('admin_email')) // Default value when calling
            )
        );
    }

    function sanitizeEmailList($input)
    {
        $emailsList = explode(',', $input);

        foreach ($emailsList as $email) {
            if (!is_email($email)) {
                add_settings_error('probemedicalext_submit_to', 'probemedicalext_submit_to_error', esc_html__('One of the email given for "Send To" is not valid.', 'probemedicalext'));
                return esc_html(get_option('probemedicalext_submit_to'));
            }
        }

        return $input;
    }

    function settingsPageContent()
    { ?>
        <div class="wrap">
            <h1><?php esc_html_e('Probe Medical Extension', 'probemedicalext'); ?></h1>
            <hr>
            <h2><?php esc_html_e('Settings', 'probemedicalext'); ?></h2>
            <ul>
                <li>
                    <a href="<?php menu_page_url('probemedicalext-mail-settings-page', true) ?>">
                        <?php esc_html_e('Mail Settings', 'probemedicalext'); ?>
                    </a>
                </li>
            </ul>
        </div>
    <?php }

    function mailSettingsPageContent()
    { ?>
        <div class="wrap">
            <h1><?php esc_html_e('Probe Medical Extension', 'probemedicalext'); ?> &#187; <small><?php esc_html_e('Mail Settings', 'probemedicalext'); ?></small></h1>
            <hr>
            <form action="options.php" method="POST">
                <?php
                settings_errors();
                settings_fields('probemedicalext_mail_group');
                do_settings_sections('probemedicalext-mail-settings-page');
                submit_button();
                ?>
            </form>
        </div>
    <?php }

    function mailSendToHTML()
    { ?>
        <input type="email" name="probemedicalext_submit_to" value="<?php echo esc_attr(get_option('probemedicalext_submit_to')); ?>" style="width: 100%" multiple />
        <p class="description"><?php esc_html_e('Add each email address separated by a coma Eg: jonh_doe@mail.com, jonhdoe2000@mail.com', 'probemedicalext'); ?></p>
<?php }
}

$probeMedicalExtensionMenuPages = new ProbeMedicalExtensionMenuPages();
