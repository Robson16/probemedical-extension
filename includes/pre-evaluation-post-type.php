<?php

/**
 * Register Custom Post Type
 */

function probemedicalext_pre_evaluation_post_type()
{
    $labels = array(
        'name'                  => esc_html_x('Pre Evaluation inbound', 'Post Type General Name', 'probemedicalext'),
        'singular_name'         => esc_html_x('Pre Evaluation inbound', 'Post Type Singular Name', 'probemedicalext'),
    );
    $args = array(
        'label'                 => esc_html__('Pre Evaluation', 'probemedicalext'),
        'description'           => esc_html__('Equipments for Pre Evaluation', 'probemedicalext'),
        'labels'                => $labels,
        'supports'              => array('title', 'editor'),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => 'probemedicalext-plugin-options-page',
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-awards',
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type('pre_evaluation', $args);
}
add_action('init', 'probemedicalext_pre_evaluation_post_type', 0);
