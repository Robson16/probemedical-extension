<?php

/**
 * Register Custom Meta Boxes
 */

function your_prefix_register_meta_boxes($meta_boxes)
{
  $prefix = 'probemedicalext-pre-evaluation';

  $meta_boxes[] = [
    'title'      => esc_html__('Pre Evaluation Inbound Fields', 'probemedicalext'),
    'id'         => 'probemedicalext-pre-evaluation-inbound',
    'post_types' => ['pre_evaluation'],
    'context'    => 'normal',
    'fields'     => [

      [
        'type' => 'textarea',
        'name' => esc_html__('Fields in JSON', 'probemedicalext'),
        'id'   => $prefix . '-inbound-fields-json',
      ],
    ],
  ];

  return $meta_boxes;
}
add_filter('rwmb_meta_boxes', 'your_prefix_register_meta_boxes');
