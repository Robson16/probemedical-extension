<h1 align="center">
  Probe Medical Extension
</h1>

Plugin to extend functionalities for the Probe Medical Theme:

- A custom block to make a pre evaluation form for the medical equipment;
- A custom WordPress API EndPoint that use the intern wordpress function to send a email;

## 🛠 Technologies
This project was developed with the following technologies

- [WordPress](https://br.wordpress.org/)
- [Node.js](https://nodejs.org/)
- [React.js](https://pt-br.reactjs.org/)
- [SASS](https://sass-lang.com/)

## :closed_book: Getting Started

You need to have [Node.js](https://nodejs.org/) installed, so you can edit and compile the custom block code, then you can clone and run this project on your WordPress installation with the instructions below:

Below you will find some information on how to run scripts.

## 👉  `npm start`
- Use to compile and run the block in development mode.
- Watches for any changes and reports back any errors in your code.

## 👉  `npm run build`
- Use to build production code for your block inside `build` folder.
- Runs once and reports back the gzip file sizes of the produced code.

## 👉  `npm format`
- It helps to enforce coding style guidelines for your files (JavaScript, YAML) by formatting source code in a consistent way.

---

### ☕❤

Robson H. Rodrigues
