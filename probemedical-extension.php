<?php

/**
 * Plugin Name:       Probe Medical Extension
 * Plugin URI:        https://gitlab.com/Robson16/probemedical-extension
 * Description:       Plugin to extend functionalities for the Probe Medical Theme
 * Version:           1.3
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Robson H. Rodrigues
 * Author URI:        https://robsonhrodrigues.com.br/
 * Text Domain:       probemedicalext
 * Domain Path:       /languages
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

define('PME_REVISION', '1.0');
define('PME_PLUGIN_URL', plugin_dir_url(__FILE__));
define('PME_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PME_PLUGIN_SLUG_PATH', plugin_basename(__FILE__));

/**
 * Automatically load dependencies and version
 */
define('PME_BLOCKS_ASSET', include PME_PLUGIN_PATH . '/build/blocks.asset.php');
define('PME_FRONTEND_ASSET', include PME_PLUGIN_PATH . '/build/frontend.asset.php');

/**
 * Frontend Scripts
 */
function probemedicalext_frontend_scripts()
{
    wp_register_script('probemedicalext-frontend', PME_PLUGIN_URL . '/build/frontend.js', PME_FRONTEND_ASSET['dependencies'], PME_FRONTEND_ASSET['version'], true);
    wp_enqueue_script('probemedicalext-frontend');

    // Scripts Translation.
    wp_set_script_translations('probemedicalext-frontend', 'probemedicalext', PME_PLUGIN_PATH . 'languages');
}
add_action('wp_enqueue_scripts', 'probemedicalext_frontend_scripts');

/**
 * Pre Evaluation Form Block Register
 */
function preevaluationform_block_init()
{
    wp_register_style('probemedicalext-blocks-style', PME_PLUGIN_URL . '/build/style-blocks.css', array('wp-editor'), PME_BLOCKS_ASSET['version'], 'all');
    wp_register_style('probemedicalext-blocks-editor', PME_PLUGIN_URL . '/build/blocks.css', array('wp-edit-blocks'), PME_BLOCKS_ASSET['version'], 'all');
    wp_register_script('probemedicalext-blocks', PME_PLUGIN_URL . '/build/blocks.js', PME_BLOCKS_ASSET['dependencies'], PME_BLOCKS_ASSET['version'], true);

    // Translation for the script of the block
    wp_set_script_translations('probemedicalext-blocks', 'probemedicalext', PME_PLUGIN_PATH . 'languages');

    register_block_type('pme/pre-evaluation-form', array(
        'style'         => 'probemedicalext-blocks-style',
        'editor_style'  => 'probemedicalext-blocks-editor',
        'editor_script' => 'probemedicalext-blocks',
    ));
}
add_action('init', 'preevaluationform_block_init');

/**
 * Plugin Setup
 */
function probemedicalext_setup()
{
    // Load translation
    load_plugin_textdomain('probemedicalext', false, dirname(PME_PLUGIN_SLUG_PATH) . '/languages');
};
add_action('init', 'probemedicalext_setup');


/**
 *  TGM Plugin
 */
require_once PME_PLUGIN_PATH . '/includes/required-plugins.php';

/**
 * Custom REST API Routes
 */
require_once PME_PLUGIN_PATH . '/includes/sendmail-route.php';

/**
 * Custom admin menu options/settings page
 */
require_once PME_PLUGIN_PATH . '/includes/admin-menu.php';

/**
 * Custom Post Types
 */
// require_once PME_PLUGIN_PATH . '/includes/pre-evaluation-post-type.php';

/**
 * Custom Meta Boxes
 */
require_once PME_PLUGIN_PATH . '/includes/metaboxes.php';
