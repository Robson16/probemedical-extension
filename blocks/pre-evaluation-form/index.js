/**
 * BLOCK: pre-evaluation-form
 */
import { v4 as uuid } from 'uuid';
import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';

import edit from './edit.js';
import save from './save.js';
import './index.scss'; // This is for styles used only in the editor.
import './style.scss'; // This is for styles used both on the front-end and in the editor.

/**
 * Register: Gutenberg Block.
 */
registerBlockType( 'pme/pre-evaluation-form', {
	title: __( 'Pre Evaluation Form', 'probemedicalext' ), // Block title.
	icon: 'forms', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'form', 'probemedicalext' ),
		__( 'pre-evaluation', 'probemedicalext' ),
	],
	supports: {
		align: [ 'wide', 'full' ],
	},
	// Block attributes
	attributes: {
		id: uuid(),
		equipmentImageId: {
			type: 'number',
			default: 0,
		},
		equipmentImageUrl: {
			type: 'string',
			default: '',
		},
		title: {
			type: 'string',
			default: '',
		},
		submitModalText: {
			type: 'string',
			default: __(
				'For more information on repairing your equipment please submit your answers and any additional notes. We will contact you to review potential solutions.',
				'probemedicalext'
			),
		},
		examplesModalOpenButtonText: {
			type: 'string',
			default: __( 'See examples', 'probemedicalext' ),
		},
		examplesModalText: {
			type: 'string',
			default: __(
				'Click on the images below to learn more about possible issues. If these areas are similar to the condition of your equipment, select the following checkboxes. These results will help our technicians diagnose your equipment.',
				'probemedicalext'
			),
		},
		questions: {
			type: 'array',
			default: [
				{
					id: uuid(),
					text: '',
					hasExamples: false,
					iconAxisX: 0,
					iconAxisY: 0,
					examples: [
						{
							id: uuid(),
							imageId: 0,
							imageUrl: '',
							label: '',
						},
					],
				},
			],
		},
	},
	edit,
	save,
} );
