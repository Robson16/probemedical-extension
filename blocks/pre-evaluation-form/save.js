import { useBlockProps } from '@wordpress/block-editor';
import { Button } from '@wordpress/components';
import { Fragment } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import classnames from 'classnames';
import EquipmentImage from './components/EquipmentImage';
import SubmitModal from './components/SubmitModal';

const PreEvaluationFormSave = ( { attributes, style } ) => {
	const {
		id,
		title,
		submitModalText,
		examplesModalOpenButtonText,
		examplesModalText,
		questions,
	} = attributes;

	const className = classnames( {
		'pme-pre-evaluation': true,
	} );

	return (
		<div
			{ ...useBlockProps.save( {
				className,
				style,
			} ) }
		>
			<div className="col">
				<h2 className="title">{ title }</h2>

				<form>
					<input type="hidden" name="title" value={ title } />
					<input
						type="hidden"
						name="questions-count"
						value={ questions.length }
					/>

					{ questions.map( ( question, questionIndex ) => (
						<div className="question-item">
							<span className="question-text">
								{ questionIndex + 1 } - { question.text }
							</span>

							<input
								type="hidden"
								name={ `question[${
									questionIndex + 1
								}][text]` }
								value={ question.text }
							/>

							<div className="question-answers">
								<label
									htmlFor={ `question-${ question.id }-answer-yes` }
									className="question-answer__container"
								>
									<input
										type="radio"
										id={ `question-${ question.id }-answer-yes` }
										className="question-answer__input"
										name={ `question[${
											questionIndex + 1
										}][answer]` }
										value="yes"
										data-target-icon={ `icon-${ question.id }` }
										required="required"
									/>
									<span className="question-answer__checkmark positive">
										{ __( 'Yes', 'probemedicalext' ) }
									</span>
								</label>

								<label
									htmlFor={ `question-${ question.id }-answer-no` }
									className="question-answer__container"
								>
									<input
										type="radio"
										id={ `question-${ question.id }-answer-no` }
										className="question-answer__input"
										name={ `question[${
											questionIndex + 1
										}][answer]` }
										value="no"
										data-target-icon={ `icon-${ question.id }` }
										required="required"
									/>
									<span className="question-answer__checkmark negative">
										{ __( 'No', 'probemedicalext' ) }
									</span>
								</label>
							</div>

							{ question.hasExamples && (
								<Fragment>
									<Button
										isLink
										className="question-examples-link"
										data-pme-toggle="modal"
										data-pme-target={ `example-modal-${ question.id }` }
									>
										{ examplesModalOpenButtonText }
									</Button>

									<div
										id={ `example-modal-${ question.id }` }
										className="pme-modal question-examples-modal"
									>
										<div className="pme-modal__dialog">
											<header className="pme-modal__header">
												<span className="question-examples-modal__number">
													{ questionIndex + 1 }
												</span>
												<Button
													className="pme-modal__close"
													data-pme-toggle="modal"
													data-pme-target={ `example-modal-${ question.id }` }
												/>
											</header>

											<div className="pme-modal__content">
												<p> { examplesModalText } </p>

												<div className="question-examples">
													{ question.examples.map(
														(
															example,
															exampleIndex
														) => (
															<label
																htmlFor={ `question-${
																	questionIndex +
																	1
																}-example-${
																	exampleIndex +
																	1
																}` }
																className="question-example"
															>
																<span className="question-example__label">
																	{
																		example.label
																	}
																</span>
																<div className="question-example__image">
																	<img
																		src={
																			example.imageUrl
																		}
																		alt={
																			example.label
																		}
																	/>
																</div>

																<input
																	className="question-example__input"
																	type="checkbox"
																	name={ `question[${
																		questionIndex +
																		1
																	}][example]` }
																	id={ `question-${
																		questionIndex +
																		1
																	}-example-${
																		exampleIndex +
																		1
																	}` }
																	value={
																		example.label
																	}
																/>
																<span className="question-example__checkmark"></span>
															</label>
														)
													) }
												</div>

												<Button
													className="pme-modal__content__close"
													data-pme-toggle="modal"
													data-pme-target={ `example-modal-${ question.id }` }
												>
													{ __(
														'Done',
														'probemedicalext'
													) }
												</Button>
											</div>
										</div>
									</div>
								</Fragment>
							) }
						</div>
					) ) }

					<SubmitModal formId={ id } headerText={ submitModalText } />
				</form>
			</div>
			<div className="col">
				<EquipmentImage
					data-related-form={ id }
					attributes={ attributes }
				/>
			</div>
		</div>
	);
};

export default PreEvaluationFormSave;
