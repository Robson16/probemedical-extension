import {
	Button,
	CheckboxControl,
	Flex,
	FlexBlock,
	Icon,
	TextControl,
} from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import { v4 as uuid } from 'uuid';
import Example from './Example';
import { useState } from 'react';

const Question = ( { id, index, attributes, setAttributes, moveQuestion } ) => {
	const { questions } = attributes;
	const [ accordionIsOpen, setAccordionIsOpen ] = useState( false );

	const question = questions.find( ( question ) => question.id === id );

	const deleteQuestion = () => {
		if (
			window.confirm(
				__( 'Are you sure you want to delete', 'probemedicalext' )
			)
		) {
			const updatedQuestions = questions.filter( ( question ) => {
				return question.id !== id;
			} );

			setAttributes( { questions: updatedQuestions } );
		}
	};

	const updateQuestionText = ( newQuestionText ) => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === id ) {
				return {
					...question,
					text: newQuestionText,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const updateQuestionHasExamples = ( newQuestionHasExamples ) => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === id ) {
				return {
					...question,
					hasExamples: newQuestionHasExamples,
					examples: question.examples
						? question.examples
						: [
								{
									id: uuid(),
									imageId: 0,
									imageUrl: '',
									label: '',
								},
						  ], // Make sure that have at least one empty object
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const updateQuestionIconAxisX = ( newIconAxisX ) => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === id ) {
				return {
					...question,
					iconAxisX: newIconAxisX,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const updateQuestionIconAxisY = ( newIconAxisY ) => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === id ) {
				return {
					...question,
					iconAxisY: newIconAxisY,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const handleToggleAccordion = () => {
		setAccordionIsOpen( ! accordionIsOpen );
	};

	return (
		<div className="pme-accordion">
			<header className="pme-accordion__header">
				<div className="pme-accordion__move">
					<Button
						className="move-up"
						onClick={ () => moveQuestion( index, 'up' ) }
					>
						<Icon
							icon="arrow-up-alt2"
							title={ __( 'Move up', 'probemedicalext' ) }
						/>
					</Button>
					<Button
						className="move-down"
						onClick={ () => moveQuestion( index, 'down' ) }
					>
						<Icon
							icon="arrow-down-alt2"
							title={ __( 'Move down', 'probemedicalext' ) }
						/>
					</Button>
				</div>
				<Button
					className="pme-accordion__toggle"
					onClick={ handleToggleAccordion }
				>
					<span>
						{ `${ __( 'Question ', 'probemedicalext' ) } ${
							index + 1
						}:` }
					</span>
					&nbsp;
					{ question.text }
				</Button>

				<Button isLink isDestructive onClick={ deleteQuestion }>
					<Icon
						icon="trash"
						title={ __( 'Delete question', 'probemedicalext' ) }
					/>
				</Button>
			</header>

			<div
				className={ `pme-accordion__content ${
					accordionIsOpen ? 'open' : ''
				}` }
			>
				<TextControl
					label={ `${ __( 'Question ', 'probemedicalext' ) } ${
						index + 1
					}:` }
					className="edit-question-text"
					value={ question.text }
					onChange={ updateQuestionText }
				/>

				<CheckboxControl
					className="edit-has-examples"
					label={ __( 'Has examples?', 'probemedicalext' ) }
					checked={ question.hasExamples }
					onChange={ ( value ) => updateQuestionHasExamples( value ) }
				/>

				<div className="edit-icon-coordinates">
					<span>
						{ __(
							'Coordinates for Icons on the Image',
							'probemedicalext'
						) }
					</span>
					<Flex>
						<FlexBlock>
							<TextControl
								label={ __( 'X axis:', 'probemedicalext' ) }
								type="number"
								value={ question.iconAxisX }
								onChange={ updateQuestionIconAxisX }
								min={ 0 }
							/>
						</FlexBlock>
						<FlexBlock>
							<TextControl
								label={ __( 'Y axis:', 'probemedicalext' ) }
								type="number"
								value={ question.iconAxisY }
								onChange={ updateQuestionIconAxisY }
								min={ 0 }
							/>
						</FlexBlock>
					</Flex>
				</div>

				{ question.hasExamples && (
					<div className="edit-examples">
						<span>{ __( 'Examples:', 'probemedicalext' ) }</span>

						{ question.examples.map( ( example ) => (
							<Example
								key={ example.id }
								id={ example.id }
								questionId={ question.id }
								attributes={ attributes }
								setAttributes={ setAttributes }
							/>
						) ) }
					</div>
				) }
			</div>
		</div>
	);
};

export default Question;
