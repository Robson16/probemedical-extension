import { useBlockProps } from '@wordpress/block-editor';
import { Icon } from '@wordpress/components';
import { Fragment } from '@wordpress/element';
import classnames from 'classnames';

const EquipmentImage = ( { attributes, style } ) => {
	const { equipmentImageUrl, title, questions } = attributes;

	const className = classnames( {
		'equipment-image': true,
	} );

	return (
		<div
			{ ...useBlockProps.save( {
				className,
				style,
			} ) }
		>
			{ ! equipmentImageUrl ? (
				<div className="equipment-image-placeholder">
					<Icon icon="format-image" />
				</div>
			) : (
				<Fragment>
					<img src={ equipmentImageUrl } alt={ title } />
					{ questions.map( ( question, index ) => {
						return (
							<span
								id={ `icon-${ question.id }` }
								style={ {
									top: `${ question.iconAxisY }px`,
									left: `${ question.iconAxisX }px`,
								} }
								className="icon"
							>
								{ index + 1 }
							</span>
						);
					} ) }
				</Fragment>
			) }
		</div>
	);
};

export default EquipmentImage;
