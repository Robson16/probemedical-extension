import {
	InspectorControls,
	MediaUpload,
	MediaUploadCheck,
} from '@wordpress/block-editor';
import { Button, PanelBody, ResponsiveWrapper } from '@wordpress/components';
import { withSelect } from '@wordpress/data';
import { Fragment } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import EquipmentImage from './EquipmentImage';

const EditComponent = ( { attributes, setAttributes, media } ) => {
	const { equipmentImageId, equipmentImageUrl } = attributes;

	const ALLOWED_MEDIA_TYPES = [ 'image' ];

	const onSelectEquipmentImage = ( media ) => {
		setAttributes( {
			equipmentImageId: media.id,
			equipmentImageUrl: media.url,
		} );
	};

	const removeEquipmentImage = () => {
		setAttributes( {
			equipmentImageId: 0,
			equipmentImageUrl: '',
		} );

		media = undefined;
	};

	const renderEquipmentImage = ( { open } ) => (
		<Fragment>
			<Button
				className={
					equipmentImageId === 0
						? 'editor-post-featured-image__toggle'
						: 'editor-post-featured-image__preview'
				}
				onClick={ open }
			>
				{ equipmentImageId === 0 ? (
					__( 'Choose an image', 'probemedicalext' )
				) : media !== undefined ? (
					<ResponsiveWrapper
						naturalWidth={ media.media_details.width }
						naturalHeight={ media.media_details.height }
					>
						<img src={ media.source_url } />
					</ResponsiveWrapper>
				) : (
					__( 'Loading...', 'probemedicalext' )
				) }
			</Button>
			{ ( media !== undefined || media !== 0 ) && (
				<Fragment>
					<br />
					<Button isSecondary onClick={ open }>
						{ __( 'Replace image', 'probemedicalext' ) }
					</Button>
					<Button
						isLink
						isDestructive
						onClick={ removeEquipmentImage }
					>
						{ __( 'Remove image', 'probemedicalext' ) }
					</Button>
				</Fragment>
			) }
		</Fragment>
	);

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody
					title={ __(
						'Select the equipment image',
						'probemedicalext'
					) }
					initialOpen={ true }
				>
					<MediaUploadCheck>
						<MediaUpload
							onSelect={ onSelectEquipmentImage }
							allowedTypes={ ALLOWED_MEDIA_TYPES }
							value={ equipmentImageId }
							render={ renderEquipmentImage }
						/>
					</MediaUploadCheck>
				</PanelBody>
			</InspectorControls>

			<EquipmentImage attributes={ attributes } />
		</Fragment>
	);
};

const EditEquipmentImage = withSelect( ( select, { attributes } ) => {
	const { equipmentImageId } = attributes;
	return {
		media: equipmentImageId
			? select( 'core' ).getMedia( equipmentImageId )
			: undefined,
	};
} )( EditComponent );

export default EditEquipmentImage;
