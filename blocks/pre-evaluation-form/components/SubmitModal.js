import { Button } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

const SubmitModal = ( { formId, headerText } ) => {
	return (
		<div
			id={ `submit-modal-${ formId }` }
			className="pme-modal submit-modal"
		>
			<div className="pme-modal__dialog">
				<header className="pme-modal__header">
					<span></span>
					<Button
						className="pme-modal__close"
						data-pme-toggle="modal"
						data-pme-target={ `submit-modal-${ formId }` }
					/>
				</header>

				<div className="pme-modal__content">
					<p> { headerText } </p>
					<div className="contact-infos">
						<label htmlFor="your-name">
							{ __( 'Name:', 'probemedicalext' ) }
							<input
								type="text"
								name="your-name"
								id="your-name"
								required="required"
							/>
						</label>
						<label htmlFor="your-company">
							{ __( 'Company:', 'probemedicalext' ) }
							<input
								type="text"
								name="your-company"
								id="your-company"
								required="required"
							/>
						</label>
						<label htmlFor="your-email">
							{ __( 'Email:', 'probemedicalext' ) }
							<input
								type="email"
								name="your-email"
								id="your-email"
								required="required"
							/>
						</label>
						<label htmlFor="your-phone">
							{ __( 'Phone:', 'probemedicalext' ) }
							<input
								type="tel"
								name="your-phone"
								id="your-phone"
								required="required"
							/>
						</label>
						<label htmlFor="your-equipment-serial-cod">
							{ __(
								'Equipment Serial Code:',
								'probemedicalext'
							) }
							<input
								type="text"
								name="your-equipment-serial-cod"
								id="your-equipment-serial-cod"
							/>
						</label>
						<label htmlFor="your-message">
							{ __( 'Message:', 'probemedicalext' ) }
							<textarea
								type="tel"
								name="your-message"
								id="your-message"
							></textarea>
						</label>
					</div>
					<input
						type="submit"
						value={ __( 'Send', 'probemedicalext' ) }
					/>
					<span class="spinner"></span>
					<div class="response-output" aria-hidden="true"></div>
				</div>
			</div>
		</div>
	);
};

export default SubmitModal;
