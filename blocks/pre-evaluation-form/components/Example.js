import { MediaUpload, MediaUploadCheck } from '@wordpress/block-editor';
import {
	Button,
	Flex,
	FlexBlock,
	FlexItem,
	Icon,
	TextControl,
} from '@wordpress/components';
import { __ } from '@wordpress/i18n';
import { v4 as uuid } from 'uuid';

const Example = ( { id, questionId, attributes, setAttributes } ) => {
	const { questions } = attributes;
	const question = questions.find(
		( question ) => question.id === questionId
	);
	const example = question.examples.find( ( example ) => example.id === id );

	const ALLOWED_MEDIA_TYPES = [ 'image' ];

	const addNewExampleToQuestion = () => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === questionId ) {
				return {
					...question,
					examples: question.examples.concat( {
						id: uuid(),
						imageId: 0,
						imageUrl: undefined,
						label: '',
					} ),
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const deleteExampleFromQuestion = () => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === questionId ) {
				const updatedExamples = question.examples.filter(
					( example ) => example.id !== id
				);

				return {
					...question,
					examples: updatedExamples,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const onSelectExampleImage = ( media ) => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === questionId ) {
				const updatedExamples = question.examples.map( ( example ) => {
					if ( example.id === id ) {
						return {
							...example,
							imageId: media.id,
							imageUrl: media.url,
						};
					} else {
						return example;
					}
				} );

				return {
					...question,
					examples: updatedExamples,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const removeExampleImage = () => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === questionId ) {
				const updatedExamples = question.examples.map( ( example ) => {
					if ( example.id === id ) {
						return {
							...example,
							imageId: 0,
							imageUrl: undefined,
						};
					} else {
						return example;
					}
				} );

				return {
					...question,
					examples: updatedExamples,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	const renderExampleImage = ( { open } ) => {
		return (
			<div className="edit-example-image">
				<Button className="edit-example-image-preview" onClick={ open }>
					{ example.imageId === 0 ? (
						__( 'Choose an image', 'probemedicalext' )
					) : example.imageUrl !== undefined ? (
						<img src={ example.imageUrl } />
					) : (
						__( 'Loading...', 'probemedicalext' )
					) }
				</Button>
				{ ( example.imageId !== 0 ||
					example.imageUrl !== undefined ) && (
					<div className="edit-example-image-actions">
						<Button isLink onClick={ open }>
							<Icon
								icon="update"
								title={ __(
									'Replace image',
									'probemedicalext'
								) }
							/>
						</Button>
						<Button isLink onClick={ removeExampleImage }>
							<Icon
								icon="trash"
								title={ __(
									'Remove image',
									'probemedicalext'
								) }
							/>
						</Button>
					</div>
				) }
			</div>
		);
	};

	const updateExampleLabelText = ( newLabelText ) => {
		const updatedQuestions = questions.map( ( question ) => {
			if ( question.id === questionId ) {
				const updatedExamples = question.examples.map( ( example ) => {
					if ( example.id === id ) {
						return {
							...example,
							label: newLabelText,
						};
					} else {
						return example;
					}
				} );

				return {
					...question,
					examples: updatedExamples,
				};
			} else {
				return question;
			}
		} );

		setAttributes( { questions: updatedQuestions } );
	};

	return (
		<Flex className="edit-example">
			<FlexItem>
				<MediaUploadCheck>
					<MediaUpload
						onSelect={ onSelectExampleImage }
						allowedTypes={ ALLOWED_MEDIA_TYPES }
						value={ example.imageId }
						render={ renderExampleImage }
					/>
				</MediaUploadCheck>
			</FlexItem>
			<FlexBlock>
				<TextControl
					label={ __( 'Label', 'probemedicalext' ) }
					value={ example.label }
					onChange={ updateExampleLabelText }
				/>
			</FlexBlock>
			<FlexItem className="example-actions">
				<Button
					isLink
					isDestructive
					onClick={ deleteExampleFromQuestion }
				>
					<Icon
						icon="trash"
						title={ __( 'Delete example', 'probemedicalext' ) }
					/>
				</Button>
				<Button isLink isPrimary onClick={ addNewExampleToQuestion }>
					<Icon
						icon="plus"
						title={ __( 'Add new example', 'probemedicalext' ) }
					/>
				</Button>
			</FlexItem>
		</Flex>
	);
};

export default Example;
