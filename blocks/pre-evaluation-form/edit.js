import { InspectorControls, useBlockProps } from '@wordpress/block-editor';
import {
	Button,
	PanelBody,
	TextareaControl,
	TextControl,
} from '@wordpress/components';
import { Fragment } from '@wordpress/element';
import { __ } from '@wordpress/i18n';
import classnames from 'classnames';
import { v4 as uuid } from 'uuid';
import EditEquipmentImage from './components/EditEquipmentImage';
import Question from './components/Question';

const PreEvaluationFormEdit = ( { attributes, setAttributes, style } ) => {
	const {
		title,
		submitModalText,
		examplesModalText,
		examplesModalOpenButtonText,
		questions,
	} = attributes;

	const blockProps = useBlockProps( {
		className: classnames( {
			'pme-pre-evaluation': true,
		} ),
		style,
	} );

	const moveQuestion = ( index, direction ) => {
		let toIndex = undefined;

		if ( direction === 'up' && index !== 0 ) {
			toIndex = index - 1;
		}

		if ( direction === 'down' && index !== questions.length ) {
			toIndex = index + 1;
		}

		if ( toIndex !== undefined ) {
			const question = questions[ index ];
			let updatedQuestions = questions
				.slice( 0, index )
				.concat( questions.slice( index + 1 ) );

			updatedQuestions.splice( toIndex, 0, question );

			setAttributes( { questions: updatedQuestions } );
		}
	};

	const addNewQuestion = () => {
		setAttributes( {
			questions: questions.concat( {
				id: uuid(),
				text: '',
				hasExamples: false,
				iconAxisX: 0,
				iconAxisY: 0,
				examples: [
					{
						id: uuid(),
						imageId: 0,
						imageUrl: undefined,
						label: '',
					},
				],
			} ),
		} );
	};

	const updateTitle = ( newTitle ) => {
		setAttributes( { title: newTitle } );
	};

	const updateSubmitModalText = ( newText ) => {
		setAttributes( { submitModalText: newText } );
	};

	const updateExamplesModalText = ( newText ) => {
		setAttributes( { examplesModalText: newText } );
	};

	const updateExamplesModalOpenButtonText = ( newText ) => {
		setAttributes( { examplesModalOpenButtonText: newText } );
	};

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody
					title={ __( 'Submit Modal', 'probemedicalext' ) }
					initialOpen={ false }
				>
					<TextareaControl
						label={ __( 'Text:', 'probemedicalext' ) }
						value={ submitModalText }
						onChange={ updateSubmitModalText }
					/>
				</PanelBody>
				<PanelBody
					title={ __( 'Examples Modal', 'probemedicalext' ) }
					initialOpen={ false }
				>
					<TextControl
						className="edit-title"
						label={ __( 'Open button:', 'probemedicalext' ) }
						value={ examplesModalOpenButtonText }
						onChange={ updateExamplesModalOpenButtonText }
					/>
					<TextareaControl
						label={ __( 'Text:', 'probemedicalext' ) }
						value={ examplesModalText }
						onChange={ updateExamplesModalText }
					/>
				</PanelBody>
			</InspectorControls>

			<div { ...blockProps }>
				<div className="col">
					<TextControl
						className="edit-title"
						label={ __( 'Title:', 'probemedicalext' ) }
						value={ title }
						onChange={ updateTitle }
					/>

					<p>
						<strong>
							{ __( 'Questions: ', 'probemedicalext' ) }
						</strong>
					</p>

					{ questions.map( ( question, index ) => (
						<Question
							key={ question.id }
							id={ question.id }
							index={ index }
							attributes={ attributes }
							setAttributes={ setAttributes }
							moveQuestion={ moveQuestion }
						/>
					) ) }

					<Button onClick={ addNewQuestion } isPrimary>
						{ __( 'Add question', 'probemedicalext' ) }
					</Button>
				</div>

				<div className="col">
					<EditEquipmentImage
						attributes={ attributes }
						setAttributes={ setAttributes }
					/>
				</div>
			</div>
		</Fragment>
	);
};

export default PreEvaluationFormEdit;
